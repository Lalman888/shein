import React from "react"

import { Button } from "@/components/ui/button"

const Login = () => {
  return (
    <>
      <div className="mb-5 rounded-lg p-6 shadow-account-shadow">
        <div className="mb-6">
          <h2 className="heading_2 mb-3">Login</h2>
          <p className="text-sm font-semibold text-primary">
            Login if you area a returning customer.
          </p>
        </div>
        <form>
          <div className="mb-4">
            <input
              className="w-full rounded-lg border-2 border-border px-4 py-2 outline-secondary focus:border-secondary"
              type="email"
              placeholder="Email Address"
              autoComplete="off"
            />
          </div>
          <div className="mb-4">
            <input
              className="w-full rounded-lg border-2 border-border px-4 py-2 outline-secondary focus:border-secondary"
              type="password"
              placeholder="Password"
            />
          </div>
          <div className="mb-4 flex justify-between">
            <div className="flex items-center ">
              <input
                type="checkbox"
                className="h-4 w-4 rounded-sm border border-gray-400 checked:bg-secondary checked:text-white focus:outline-none"
              />
              <label className="ml-2 text-sm font-bold text-primary">
                Remember me
              </label>
            </div>
            <div>
              <p className="text-lg font-semibold text-secondary">
                Forgot Password?
              </p>
            </div>
          </div>
          <div className="mb-4">
            <Button
              variant="secondary"
              size="lg"
              className="button_secondary w-full"
              style={{ textTransform: "capitalize" }}
            >
              Login
            </Button>
          </div>
          <div className="mb-4">
            <div className="relative flex justify-center text-sm">
              <span className="bg-white px-2 text-gray-500">OR</span>
            </div>
          </div>
          <div className="mb-4 flex justify-center gap-x-2">
            <span className="cursor-pointer rounded bg-[#4867AA] px-8 py-2 text-white hover:bg-secondary">
              Facebook
            </span>
            <span className="cursor-pointer rounded bg-red-600  px-8 py-2 text-white hover:bg-secondary">
              Google
            </span>
            <span className="cursor-pointer rounded bg-sky-500  px-8 py-2 text-white hover:bg-secondary">
              Twitter
            </span>
          </div>
          <div className="mb-3">
            <p className="text-center text-lg font-semibold text-primary">
              Don{"'t"} have an account?{" "}
              <span className="text-secondary">Sign up now</span>
            </p>
          </div>
        </form>
      </div>
    </>
  )
}

export default Login
