import React from "react"

import { Button } from "@/components/ui/button"

const Register = () => {
  return (
    <>
      <div className="mb-5 rounded-lg p-6 shadow-account-shadow">
        <div className="mb-6">
          <h2 className="heading_2 mb-3">Create an Account</h2>
          <p className="text-sm font-semibold text-primary">
            Register here if you are a new customer
          </p>
        </div>
        <form>
          <div className="mb-4">
            <input
              className="w-full rounded-lg border-2 border-border px-4 py-2 outline-secondary focus:border-secondary"
              type="text"
              placeholder="Username"
              autoComplete="off"
            />
          </div>
          <div className="mb-4">
            <input
              className="w-full rounded-lg border-2 border-border px-4 py-2 outline-secondary focus:border-secondary"
              type="email"
              placeholder="Email Address"
              autoComplete="off"
            />
          </div>
          <div className="mb-4">
            <input
              className="w-full rounded-lg border-2 border-border px-4 py-2 outline-secondary focus:border-secondary"
              type="password"
              placeholder="Password"
            />
          </div>
          <div className="mb-4">
            <input
              className="w-full rounded-lg border-2 border-border px-4 py-2 outline-secondary focus:border-secondary"
              type="text"
              placeholder="Confirm Password"
            />
          </div>
          <div className="mb-4">
            <Button
              variant="secondary"
              size="lg"
              className="button_secondary w-full"
              style={{ textTransform: "capitalize" }}
            >
              Submit & Register
            </Button>
          </div>
          <div className="mb-4 flex">
            <div className="flex items-center ">
              <input
                type="checkbox"
                className="h-4 w-4 rounded-sm border border-gray-400 checked:bg-secondary checked:text-white focus:outline-none"
              />
              <label className="ml-2 text-sm font-bold text-primary">
                I have read and agree to the terms & conditions
              </label>
            </div>
          </div>
        </form>
      </div>
    </>
  )
}

export default Register
