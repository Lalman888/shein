import Link from "next/link"

import { MainNav } from "./main-nav"
// import { ThemeToggle } from "@/components/theme/theme-toggle"
import { TopHeader } from "./top-header"

export function SiteHeader() {
  return (
    <header className="sticky top-0 z-40 w-full bg-background">
      <div className="container flex h-16 w-full py-3 ">
        <TopHeader />
      </div>
      <div className="flex justify-center bg-secondary ">
        <div className="container">
          <MainNav />
        </div>
      </div>
    </header>
  )
}
