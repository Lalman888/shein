import Link from "next/link"
import { BsSuitHeart } from "react-icons/bs"
import { FiUser } from "react-icons/fi"
import { HiOutlineSearch, HiOutlineShoppingBag } from "react-icons/hi"

export function TopHeader() {
  return (
    <>
      <div className="flex w-full items-center justify-between gap-3">
        <div className="flex items-center">
          <HiOutlineSearch className="text-2xlss cursor-pointer text-primary hover:text-secondary" />
          <input
            type="text"
            placeholder="Search......"
            className="h-12 w-48 border-none bg-white px-3 text-primary outline-none placeholder:text-primary focus:border-none focus:outline-none focus:ring-0"
          />
        </div>

        <Link href="/">
          <span className="text-3xl text-secondary">Shein</span>
        </Link>

        <div className="flex items-baseline gap-3 ">
          <Link href="/wishlist" legacyBehavior passHref>
            <BsSuitHeart className="cursor-pointer text-2xl text-primary transition-all duration-300 ease-in hover:text-secondary" />
          </Link>
          <Link href="/account" legacyBehavior passHref>
            <FiUser className="cursor-pointer text-2xl text-primary transition-all duration-300 ease-in hover:text-secondary" />
          </Link>

          <Link href="/cart" legacyBehavior passHref>
            <div className="relative">
              <HiOutlineShoppingBag className="cursor-pointer text-2xl text-primary transition-all duration-300 ease-in hover:text-secondary" />
              <span className="absolute -right-1 -top-2 h-4 w-4 rounded-full bg-primary text-center text-xs text-white">
                2
              </span>
            </div>
          </Link>
        </div>
      </div>
    </>
  )
}
