"use client"

import * as React from "react"
import Image from "next/image"
import Link from "next/link"

import { cn } from "@/lib/utils"
import {
  NavigationMenu,
  NavigationMenuContent,
  NavigationMenuIndicator,
  NavigationMenuItem,
  NavigationMenuLink,
  NavigationMenuList,
  NavigationMenuTrigger,
  NavigationMenuViewport,
  navigationMenuTriggerStyle,
} from "@/components/ui/navigation-menu"

export function MainNav() {
  return (
    <>
      <NavigationMenu className="py-3">
        <NavigationMenuList>
          <NavigationMenuItem className=" group/home">
            <NavigationMenuTrigger>Home</NavigationMenuTrigger>
            <div className="hidden group-hover/home:block">
              <NavigationMenuViewport />
            </div>
            <NavigationMenuContent>
              <ul className="p-6 md:w-[100px] lg:w-[200px] ">
                {[1, 2, 3, 4, 5, 6, 7].map((item) => (
                  <ListItem key={item} href="/" title="Home - Beauty" />
                ))}
              </ul>
            </NavigationMenuContent>
          </NavigationMenuItem>
          <NavigationMenuItem className="group/shop">
            <NavigationMenuTrigger>Shop</NavigationMenuTrigger>
            <div className="absolute top-9 hidden group-hover/shop:block">
              <NavigationMenuViewport />
            </div>
            <NavigationMenuContent>
              <div className="fixed left-0 bg-white p-5 lg:w-full ">
                <div className="container flex">
                  <div className="w-[19%] px-3 py-1">
                    <ul className="">
                      {[1, 2, 3, 4, 5, 6, 7].map((item) => (
                        <ListItem key={item} href="/" title="Shop - Beauty" />
                      ))}
                    </ul>
                  </div>
                  <div className="w-[19%] px-3 py-1">
                    <ul className="">
                      {[1, 2, 3, 4, 5, 6, 7].map((item) => (
                        <ListItem key={item} href="/" title="Shop - Beauty" />
                      ))}
                    </ul>
                  </div>
                  <div className="w-[19%] px-3 py-1">
                    <ul className="">
                      {[1, 2, 3, 4, 5, 6, 7].map((item) => (
                        <ListItem key={item} href="/" title="Shop - Beauty" />
                      ))}
                    </ul>
                  </div>
                  <div className="w-[19%] px-3 py-1">
                    <ul className="">
                      {[1, 2, 3, 4, 5, 6, 7].map((item) => (
                        <ListItem key={item} href="/" title="Shop - Beauty" />
                      ))}
                    </ul>
                  </div>
                  <div className="w-[19%] px-3 py-1">
                    <Image
                      src="/img/banner/banner-menu.webp"
                      width={546}
                      height={766}
                      alt="bn"
                    />
                  </div>
                </div>
              </div>
            </NavigationMenuContent>
          </NavigationMenuItem>
          <NavigationMenuItem>
            <Link href="/" legacyBehavior passHref>
              <NavigationMenuLink className={navigationMenuTriggerStyle()}>
                Accesories
              </NavigationMenuLink>
            </Link>
          </NavigationMenuItem>
          <NavigationMenuItem className="group/blog">
            <NavigationMenuTrigger>Blog</NavigationMenuTrigger>
            <div className="hidden group-hover/blog:block">
              <NavigationMenuViewport />
            </div>
            <NavigationMenuContent>
              <ul className="p-6 md:w-[100px] lg:w-[200px] ">
                {[1, 2, 3, 4, 5, 6, 7].map((item) => (
                  <ListItem key={item} href="/" title="Blog - Beauty" />
                ))}
              </ul>
            </NavigationMenuContent>
          </NavigationMenuItem>
          <NavigationMenuItem className="group/page">
            <NavigationMenuTrigger>Page</NavigationMenuTrigger>
            <div className="hidden group-hover/page:block">
              <NavigationMenuViewport />
            </div>
            <NavigationMenuContent>
              <ul className="p-6 md:w-[100px] lg:w-[200px] ">
                {[1, 2, 3, 4, 5, 6, 7].map((item) => (
                  <ListItem key={item} href="/" title="Page - Beauty" />
                ))}
              </ul>
            </NavigationMenuContent>
          </NavigationMenuItem>
          <NavigationMenuItem>
            <Link href="/contact" legacyBehavior passHref>
              <NavigationMenuLink className={navigationMenuTriggerStyle()}>
                Contact
              </NavigationMenuLink>
            </Link>
          </NavigationMenuItem>
        </NavigationMenuList>
      </NavigationMenu>
    </>
  )
}

const ListItem = React.forwardRef<
  React.ElementRef<"a">,
  React.ComponentPropsWithoutRef<"a">
>(({ className, title, children, ...props }, ref) => {
  return (
    <li>
      <NavigationMenuLink asChild>
        <a
          ref={ref}
          className={cn(
            "block select-none space-y-1 rounded-md p-3 leading-none no-underline outline-none transition-colors hover:text-secondary focus:text-secondary",
            className
          )}
          {...props}
        >
          <div className="text-sm font-medium leading-none">{title}</div>
        </a>
      </NavigationMenuLink>
    </li>
  )
})
ListItem.displayName = "ListItem"
