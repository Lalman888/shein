import React from "react"

const Footer = () => {
  return (
    <>
      <footer className="bg-gray-400 ">
        <div className="container py-20">
          <div className="grid grid-cols-6">
            <div className="col-span-2">
              <span className="pb-4 text-4xl text-secondary">Shein</span>
              <p className="pb-4 text-primary">
                Corporate clients and leisure travelers has been relying on
                Groundlink for dependable safe, and professional
              </p>
            </div>
            <div>
              <h3 className="pb-4 uppercase text-primary">OUR OFFER</h3>
              <ul className="text-primary">
                <li>Contact Us</li>
                <li>About Us</li>
                <li>Wishlist</li>
                <li>Privacy Policy</li>
              </ul>
            </div>
            <div>
              <h3 className="pb-4 uppercase text-primary">QUICK LINKS</h3>
              <ul className="text-primary">
                <li>My Account</li>
                <li>Shopping Cart</li>
                <li>Login</li>
                <li>Register</li>
              </ul>
            </div>
            <div className="col-span-2">
              <h3 className="pb-4 uppercase text-primary">NEWSLETTER</h3>
              <p className="text-primary">
                Subscribe to our weekly Newsletter and receive updates via
                email.
              </p>
              <div className="pt-4">
                <input
                  type="email"
                  className="rounded border-secondary bg-gray-50 py-4 pl-5 pr-32 text-primary outline-primary ring-0"
                  placeholder="Enter Your Email"
                />
              </div>
            </div>
          </div>
        </div>
      </footer>
    </>
  )
}

export default Footer
