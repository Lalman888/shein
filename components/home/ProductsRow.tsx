import React from "react"

import SmallProductCard from "@/components/cards/SmallProductCard"

const ProductsRow = () => {
  return (
    <>
      <section className="pb-20">
        <div className="container">
          <div className="flex items-center justify-between gap-4 pt-12">
            <div>
              <h2 className="heading_2 mb-6 ">Featured Products</h2>
              <SmallProductCard />
              <SmallProductCard />
              <SmallProductCard />
            </div>
            <div>
              <h2 className="heading_2 mb-6 ">Onsale Products</h2>
              <SmallProductCard />
              <SmallProductCard />
              <SmallProductCard />
            </div>
            <div>
              <h2 className="heading_2 mb-6 ">Top Products</h2>
              <SmallProductCard />
              <SmallProductCard />
              <SmallProductCard />
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default ProductsRow
