import React, { Fragment } from "react"

import ProductCard from "@/components/cards/ProductCard"

const TrendingProduct = () => {
  return (
    <>
      <section className="py-20">
        <div className="container ">
          <div className="mb-9 text-center">
            <h2 className="heading_2 uppercase">Trending Products</h2>
          </div>
          <div className="grid grid-cols-1 gap-5 md:grid-cols-2 lg:grid-cols-4 lg:gap-14">
            {[1, 2, 3, 4, 5, 6, 7, 8].map((item, index) => (
              <Fragment key={index}>
                <ProductCard />
              </Fragment>
            ))}
          </div>
        </div>
      </section>
    </>
  )
}

export default TrendingProduct
