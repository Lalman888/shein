import React from "react"
import Image from "next/image"
import { FaPlay } from "react-icons/fa"

import { Button } from "@/components/ui/button"

const About = () => {
  return (
    <>
      <section>
        <div className="container">
          <div className="flex flex-col gap-5 md:flex-row ">
            <div className="relative w-full md:w-1/2 md:pl-20">
              <Image
                src="/img/other/about.webp"
                width={550}
                height={512}
                alt="about"
              />
              <div className="border_secondary absolute left-0 top-[36%] w-40 rounded-xl bg-white px-5 py-4 text-center ">
                <span className="mb-2 inline-block text-5xl font-extrabold text-primary">
                  <span className="font-frank">15</span>+
                </span>

                <span className="inline-block text-2xl font-medium text-primary">
                  Years <br /> Experience
                </span>
              </div>
            </div>
            <div className="flex w-full flex-col justify-center gap-4 md:w-1/2 ">
              <h3 className="text-xl font-semibold text-secondary">About</h3>
              <h2 className="heading_2">Curated by color</h2>
              <p className="text-lg text-gray-500">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam,
              </p>
              <Button
                variant="secondary"
                size="lg"
                className="button_secondary w-fit"
              >
                View More
              </Button>
            </div>
          </div>
          <div className="flex flex-col-reverse gap-5 pt-7 md:flex-row">
            <div className="flex w-full flex-col justify-center gap-4 md:w-1/2">
              <h2 className="heading_2">
                We have the princess cuts to rule them all
              </h2>
              <p className="text-lg text-gray-500">
                Ehuese marws dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Quis
                ipsum suspendisse ultrices gravida. Risus commodo viverra
                maecenas accumsan lacus vel facilisis.
              </p>
              <Button
                variant="secondary"
                size="lg"
                className="button_secondary w-fit"
              >
                View More
              </Button>
            </div>
            <div className="relative w-full md:w-1/2 md:pr-20">
              <Image
                src="/img/other/about.webp"
                width={550}
                height={564}
                alt="about"
              />
              <div className="absolute left-[42%] top-[46%] px-4  text-secondary ">
                <span>
                  <FaPlay className="flex h-14 w-14 animate-bideo_play cursor-pointer items-center justify-center  rounded-full bg-[#F7EEDD] p-2 " />
                </span>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default About
