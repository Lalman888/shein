"use client"

import React, { Fragment } from "react"
import { Carousel } from "@mantine/carousel"
import { createStyles, getStylesRef } from "@mantine/core"
import { BsChevronLeft, BsChevronRight } from "react-icons/bs"

import ProductCard from "@/components/cards/ProductCard"

const useStyles = createStyles(() => ({
  controls: {
    ref: getStylesRef("controls"),
    transition: "opacity 150ms ease",
    opacity: 0,
  },

  root: {
    "&:hover": {
      [`& .${getStylesRef("controls")}`]: {
        opacity: 1,
      },
    },
  },
}))

const FeaturedProducts = () => {
  const { classes } = useStyles()
  return (
    <>
      <section className="py-20">
        <div className="container ">
          <div className="mb-9 text-center">
            <h2 className="heading_2 uppercase">Featured Products</h2>
          </div>
          <div>
            <Carousel
              maw="100%"
              mx="auto"
              height={502}
              slideSize="25%"
              classNames={classes}
              slidesToScroll={1}
              slideGap="md"
              nextControlIcon={
                <BsChevronRight className="h-8 w-8 rounded-full bg-white p-2 text-secondary transition duration-150 ease-in-out hover:bg-secondary hover:text-white" />
              }
              previousControlIcon={
                <BsChevronLeft className="h-8 w-8 rounded-full bg-white p-2 text-secondary transition duration-150 ease-in-out hover:bg-secondary hover:text-white" />
              }
              loop
              align="start"
            >
              {[1, 2, 3, 4, 5].map((item) => (
                <Fragment key={item}>
                  <Carousel.Slide>
                    <div>
                      <ProductCard />
                    </div>
                  </Carousel.Slide>
                </Fragment>
              ))}
            </Carousel>
          </div>
        </div>
      </section>
    </>
  )
}

export default FeaturedProducts
