import React from "react"
import Image from "next/image"

interface Service {
  title: string
  description: string
  icon: string
}

const services = [
  {
    title: "Free Shipping",
    description: "Free shipping on all order",
    icon: "/img/other/feature1.webp",
  },
  {
    title: "Support 24/7",
    description: "Contact us 24 hours a day",
    icon: "/img/other/feature1.webp",
  },
  {
    title: "100% Money Back",
    description: "You have 30 days to Return",
    icon: "/img/other/feature1.webp",
  },
  {
    title: "Payment Secure",
    description: "We ensure secure payment",
    icon: "/img/other/feature1.webp",
  },
]

const Services = () => {
  return (
    <section className="container py-16">
      <div className="flex flex-col items-center justify-between gap-8  md:flex-row">
        {services.map((service: Service, index: number) => (
          <>
            <div className="flex gap-x-4" key={index}>
              <div>
                <Image
                  src={service.icon}
                  alt="Picture of the author"
                  width={48}
                  height={52}
                />
              </div>
              <div className="flex flex-col gap-y-1">
                <span className="font-frank text-lg font-extrabold">
                  {service.title}
                </span>
                <p className="text-sm text-primary">{service.description}</p>
              </div>
            </div>
          </>
        ))}
      </div>
    </section>
  )
}

export default Services
