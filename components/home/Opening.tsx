import React from "react"
import Image from "next/image"

import { Button } from "@/components/ui/button"

const Opening = () => {
  return (
    <>
      <section className="pb-20">
        <div className="relative h-[580px] w-full bg-[url('/img/banner/banner-fullwidth7.webp')]">
          <div className="container">
            <div className="absolute left-0 top-0 h-full w-full bg-black/10" />
            <div className="absolute left-0 top-0 flex h-full w-full items-center justify-center">
              <div className="text-center">
                <h2 className="mb-4 text-xl font-bold text-black">
                  GRAND OPENING
                </h2>
                <h1 className="mb-4 text-4xl font-bold text-black">
                  SUMMER COLLECTIONS
                </h1>

                <p className="mb-8 text-sm text-primary">
                  Ehuese marws dolor sit amet, consectetur adipiscing elit, sed
                  do eiusmod tempor incididunt ut labore <br />
                  et dolore magna aliqua. Quis ipsum suspendisse ultrices
                  gravida.
                </p>
                <Button
                  variant="secondary"
                  size="lg"
                  className="button_secondary w-fit"
                >
                  Shop Now
                </Button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Opening
