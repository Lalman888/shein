import React from "react"
import Image from "next/image"

import { Button } from "@/components/ui/button"

const Offers = () => {
  return (
    <>
      <section className="mb-6">
        <div className="container">
          <div className="grid grid-flow-col grid-rows-4 gap-4">
            <div className="relative row-span-2">
              <Image
                src="/img/banner/banner25.webp"
                width={586}
                height={409}
                alt="offer"
              />
              {/* eslint-disable-next-line tailwindcss/migration-from-tailwind-2 */}
              {/* <div className="absolute left-0 top-0 flex h-full w-full items-center justify-center bg-black bg-opacity-50" /> */}
              <div className=" absolute right-[16%] top-[29%]">
                <h4 className="mb-3 text-lg font-semibold text-primary">
                  New Arrivals
                </h4>
                <h3 className="mb-3 text-xl text-black">
                  Rings <br /> Collection
                </h3>
                <Button
                  variant="secondary"
                  size="lg"
                  className="button_secondary h-10 w-fit px-5"
                >
                  Shop Now
                </Button>
              </div>
            </div>
            <div className="relative row-span-2">
              <Image
                src="/img/banner/banner25.webp"
                width={586}
                height={409}
                alt="offer"
              />
              {/* eslint-disable-next-line tailwindcss/migration-from-tailwind-2 */}
              {/* <div className="absolute left-0 top-0 flex h-full w-full items-center justify-center bg-black bg-opacity-50" /> */}
              <div className=" absolute left-[8%] top-[22%]">
                <h4 className="mb-3 text-lg font-semibold text-primary">
                  New Arrivals
                </h4>
                <h3 className="mb-3 text-xl text-black">
                  Rings <br /> Collection
                </h3>
                <Button
                  variant="secondary"
                  size="lg"
                  className="button_secondary h-10 w-fit px-5"
                >
                  Shop Now
                </Button>
              </div>
            </div>
            <div className="relative row-span-4">
              <Image
                src="/img/banner/banner27.webp"
                width={656}
                height={852}
                alt="offer"
              />
              {/* eslint-disable-next-line tailwindcss/migration-from-tailwind-2 */}
              {/* <div className="absolute left-0 top-0 flex h-full w-full items-center justify-center bg-black bg-opacity-50" /> */}
              <div className=" absolute left-[8%] top-[4%]">
                <h4 className="mb-3 text-lg font-semibold text-primary">
                  New Arrivals
                </h4>
                <h3 className="mb-3 text-xl text-black">
                  Rings <br /> Collection
                </h3>
                <Button
                  variant="secondary"
                  size="lg"
                  className="button_secondary h-10 w-fit px-5"
                >
                  Shop Now
                </Button>
              </div>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}

export default Offers
