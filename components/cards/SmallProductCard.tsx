"use client"

import React from "react"
import Image from "next/image"
import { Rating } from "@mantine/core"

const SmallProductCard = () => {
  return (
    <>
      <article className="mb-8 flex gap-2 ">
        <div className="relative">
          <Image
            src="/img/product/small-product/product4.webp"
            alt="product"
            width={136}
            height={115}
            className=" hover:opacity-90"
          />
        </div>

        <div className="px-4 py-5 ">
          <div className="mb-3 ">
            <h3 className="font-frank text-lg  font-semibold text-primary">
              Z 7-8mm Freshwater Button
            </h3>
          </div>
          <div className="mb-3 flex gap-x-1">
            <Rating defaultValue={4} />
            <span className="text-xs font-normal text-secondary-foreground">
              (126) Review
            </span>
          </div>

          <div className="flex ">
            <div className="flex gap-x-1">
              <span className="text-sm font-semibold text-destructive">
                $215.52
              </span>
              <span className="text-sm font-normal text-secondary-foreground line-through">
                $320.00
              </span>
            </div>
          </div>
        </div>
      </article>
    </>
  )
}

export default SmallProductCard
