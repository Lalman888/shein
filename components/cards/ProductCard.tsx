"use client"

import React from "react"
import Image from "next/image"
import Link from "next/link"
import { Rating } from "@mantine/core"
import { AiOutlineHeart, AiOutlineSearch } from "react-icons/ai"
import { BsBasket } from "react-icons/bs"
import { FiMinimize2 } from "react-icons/fi"

import { Button } from "@/components/ui/button"

const ProductCard = () => {
  return (
    <>
      <Link href="/product_details">
        <article className="group/product cursor-pointer rounded-lg shadow-md">
          <div className="relative">
            <Image
              src="/img/product/main-product/product11.webp"
              alt="product"
              width={300}
              height={292}
              className="rounded-t-lg hover:opacity-90"
            />
            <span className="absolute left-0 top-4 h-6 w-12 rounded-md bg-secondary px-2 py-1 text-center text-xs font-semibold text-white">
              -14%
            </span>
            <div className="absolute right-5 top-4 flex h-9 w-9 cursor-pointer items-center justify-center rounded-full bg-white text-secondary-foreground opacity-0 transition-opacity duration-200 ease-in-out hover:bg-secondary hover:text-white group-hover/product:opacity-100">
              <AiOutlineSearch className="text-xl" />
            </div>
            <div className="absolute right-5 top-16 flex h-9 w-9 cursor-pointer items-center justify-center rounded-full bg-white text-secondary-foreground opacity-0 transition-opacity duration-200 ease-in-out hover:bg-secondary hover:text-white group-hover/product:opacity-100">
              <FiMinimize2 className="text-xl" />
            </div>
            <div className="absolute right-5 top-28 flex h-9 w-9 cursor-pointer items-center justify-center rounded-full bg-white text-secondary-foreground opacity-0 transition-opacity duration-200 ease-in-out hover:bg-secondary hover:text-white group-hover/product:opacity-100">
              <AiOutlineHeart className="text-xl" />
            </div>
            <div className="absolute bottom-3 right-[34%] hidden justify-center transition-all duration-300 ease-in-out group-hover/product:flex ">
              <Button
                variant="secondary"
                size="vsm"
                className="button_secondary w-fit text-xs"
              >
                Add to Cart <BsBasket className="ml-2" />
              </Button>
            </div>
          </div>
          <div className="px-4 py-5 ">
            <div className="mb-3 flex justify-center gap-x-1">
              <Rating defaultValue={4} />
              <span className="text-xs font-normal text-secondary-foreground">
                (126) Review
              </span>
            </div>
            <div className="mb-3 text-center">
              <h3 className="font-frank text-lg  font-semibold text-primary">
                Z 7-8mm Freshwater Button
              </h3>
              di
            </div>
            <div className="flex justify-center">
              <div className="flex gap-x-1">
                <span className="text-sm font-semibold text-destructive">
                  $215.52
                </span>
                <span className="text-sm font-normal text-secondary-foreground line-through">
                  $320.00
                </span>
              </div>
            </div>
          </div>
        </article>
      </Link>
    </>
  )
}

export default ProductCard
