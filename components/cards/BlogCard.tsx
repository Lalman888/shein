import React from "react"
import Image from "next/image"

const BlogCard = () => {
  return (
    <>
      <article>
        <div className="relative">
          <Image
            src="/img/blog/blog1.webp"
            alt="blog-1"
            width={402}
            height={310}
            className="rounded hover:opacity-80"
          />
        </div>
        <div className=" pt-4">
          <div className="flex gap-4 text-sm font-medium text-primary">
            <span className="">30 March, 2022</span>
            <span className=""> / </span>
            <span className="">(02) Comment</span>
          </div>
          <h3 className="my-3 font-frank text-lg font-bold hover:text-secondary">
            Justo Pellentesque Donec lobortis faucibus Vestibulum
          </h3>
          <p className="text-sm text-primary underline">Read More</p>
        </div>
      </article>
    </>
  )
}

export default BlogCard
