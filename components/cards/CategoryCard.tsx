import React from "react"
import Image from "next/image"

const CategoryCard = () => {
  return (
    <>
      <article className="group/category w-fit cursor-pointer rounded-lg">
        <div>
          <Image
            src="/img/collection/collection10.webp"
            alt="product"
            width={230}
            height={199}
            className="hover:opacity-90"
          />
        </div>
        <div className="mb-3 pt-3 text-center">
          <h3 className="font-frank text-lg  font-bold text-primary group-hover/category:text-secondary">
            Necklaces
          </h3>
        </div>
      </article>
    </>
  )
}

export default CategoryCard
