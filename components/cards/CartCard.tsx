import React from "react"
import Image from "next/image"
import { AiOutlineClose } from "react-icons/ai"

const CartCard = () => {
  return (
    <>
      <tr className="border-b border-solid border-secondary-foreground">
        <td className="py-5 pr-5">
          <div className="flex items-center">
            <button className="mr-7 flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-white font-semibold shadow-xl">
              <AiOutlineClose
                className="text-primary hover:text-secondary"
                size={18}
              />
            </button>
            <div className="w-24">
              <Image
                src="/img/product/small-product/product1.webp"
                alt="Picture of the author"
                width={80}
                height={72}
              />
            </div>
            <div className="pl-4 ">
              <h3 className="mb-1 font-frank text-sm font-bold">
                Fairness cream
              </h3>
              <span className="mb-1 text-xs font-medium text-secondary-foreground">
                COLOR: Blue
              </span>
              <p className="mb-1 text-xs font-medium text-secondary-foreground">
                WEIGHT: 2 Kg
              </p>
            </div>
          </div>
        </td>
        <td className="py-5 pr-5">
          <span className="text-sm font-semibold text-primary">£65.00</span>
        </td>
        <td className="py-5 pr-5">
          <div className="flex">
            <button
              className="mr-1 flex h-8 w-8 cursor-pointer items-center justify-center 
            rounded-l-lg bg-white font-semibold shadow-xl"
            >
              -
            </button>
            <input
              className="h-8 w-8 border border-solid border-y-border text-center text-sm"
              type="text"
              placeholder="1"
              //   value="1"
            />
            <button
              className="-mr-1 flex h-8 w-8 cursor-pointer items-center justify-center 
            rounded-r-lg bg-white font-semibold shadow-xl"
            >
              +
            </button>
          </div>
        </td>
        <td className="py-5 ">
          <span className="text-sm font-semibold text-primary">£65.00</span>
        </td>
      </tr>
    </>
  )
}

export default CartCard
