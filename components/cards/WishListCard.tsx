import React from "react"
import Image from "next/image"
import { AiOutlineClose } from "react-icons/ai"

import { Button } from "@/components/ui/button"

const WishListCard = () => {
  return (
    <>
      <tr className="border-b border-solid border-secondary-foreground">
        <td className="py-5 pr-5">
          <div className="flex items-center">
            <button className="mr-7 flex h-8 w-8 cursor-pointer items-center justify-center rounded-full bg-white font-semibold shadow-xl">
              <AiOutlineClose
                className="text-primary hover:text-secondary"
                size={18}
              />
            </button>
            <div className="w-24">
              <Image
                src="/img/product/small-product/product1.webp"
                alt="Picture of the author"
                width={80}
                height={72}
              />
            </div>
            <div className="pl-4 ">
              <h3 className="mb-1 font-frank text-sm font-bold">
                Fairness cream
              </h3>
              <span className="mb-1 text-xs font-medium text-secondary-foreground">
                COLOR: Blue
              </span>
              <p className="mb-1 text-xs font-medium text-secondary-foreground">
                WEIGHT: 2 Kg
              </p>
            </div>
          </div>
        </td>
        <td className="py-5 pr-5">
          <span className="text-sm font-semibold text-primary">£65.00</span>
        </td>
        <td className="py-5 pr-5">
          <div className="flex">
            <p className="text-secondary">In Stock</p>
          </div>
        </td>
        <td className="py-5 ">
          <Button
            variant="secondary"
            size="lg"
            className="button_secondary w-fit text-xs "
            style={{ textTransform: "capitalize", padding: "1px 20px" }}
          >
            Add to Cart
          </Button>
        </td>
      </tr>
    </>
  )
}

export default WishListCard
