"use client"

import { useRef } from "react"
import { Carousel } from "@mantine/carousel"
import Autoplay from "embla-carousel-autoplay"
import { BsArrowRight } from "react-icons/bs"

import { Button } from "@/components/ui/button"

const HeroSlider: React.FC = () => {
  const autoplay = useRef(Autoplay({ delay: 2000 }))
  return (
    <>
      <div>
        <Carousel
          slideSize="100%"
          maw="100%"
          height={550}
          loop
          withControls={false}
          withIndicators
          plugins={[autoplay.current]}
        >
          <Carousel.Slide>
            <HeroSlide />
          </Carousel.Slide>
          <Carousel.Slide>
            <HeroSlide />
          </Carousel.Slide>
          <Carousel.Slide>
            <HeroSlide />
          </Carousel.Slide>
        </Carousel>
      </div>
    </>
  )
}

export default HeroSlider

const HeroSlide = () => {
  return (
    <div className="bg-gray-300 py-16 lg:py-36 xl:py-48 2xl:py-52">
      <div className="container w-3/4 text-center">
        <h1 className="font-frank text-3xl font-extrabold text-primary lg:text-6xl">
          Jewelry to fit every budget, occasion, and <br /> taste
        </h1>
        <Button variant="secondary" size="lg" className="button_secondary mt-8">
          Shop Now <BsArrowRight className="ml-2 inline-block text-xl" />
        </Button>
      </div>
    </div>
  )
}
