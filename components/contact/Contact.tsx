import React from "react"

import { Button } from "@/components/ui/button"

const ContactUs = () => {
  return (
    <>
      <div className="flex flex-col gap-y-3">
        <div className="flex w-full gap-x-5">
          <div className="flex w-1/2 flex-col gap-y-3">
            <label className="text-primary">
              First Name <span className="text-red-600">*</span>
            </label>
            <input
              type="text"
              className="rounded-md border border-gray-200 px-4 py-3.5 focus:border-secondary focus:outline-none focus:ring-0"
              placeholder="First Name"
            />
          </div>
          <div className="flex w-1/2 flex-col gap-y-3">
            <label className="text-primary">
              Last Name <span className="text-red-600">*</span>
            </label>
            <input
              type="text"
              className="  rounded-md border border-gray-200 px-4 py-3.5 focus:border-secondary focus:outline-none"
              placeholder="Last Name"
            />
          </div>
        </div>
        <div className="flex gap-x-5">
          <div className="flex w-1/2 flex-col gap-y-3">
            <label className="text-primary">
              Phone Number <span className="text-red-600">*</span>
            </label>
            <input
              type="text"
              className="  rounded-md border border-gray-200 px-4 py-3.5 focus:border-secondary focus:outline-none"
              placeholder="Phone Number"
            />
          </div>
          <div className="flex w-1/2 flex-col gap-y-3">
            <label className="text-primary">
              Email <span className="text-red-600">*</span>
            </label>
            <input
              type="email"
              className="  rounded-md border border-gray-200 px-4 py-3.5 focus:border-secondary focus:outline-none"
              placeholder="Email"
            />
          </div>
        </div>
        <div className="flex flex-col gap-y-3">
          <label className="text-primary">
            Write Your Message <span className="text-red-600">*</span>
          </label>
          <textarea
            rows={5}
            className="  rounded-md border border-gray-200 px-4 py-3.5 focus:border-secondary focus:outline-none"
            placeholder="Write Your Message "
          />
        </div>
        <Button
          variant="secondary"
          size="lg"
          className="button_secondary mt-3 w-fit"
        >
          Submit Now
        </Button>
      </div>
    </>
  )
}

export default ContactUs
