import React from "react"

const FilterPrice = () => {
  return (
    <>
      <div className="mr-9 rounded-sm border border-solid border-border px-4 py-5 shadow-widget-shadow">
        <h2 className="widget__title mb-6 font-frank text-base font-semibold">
          Filter By Price
        </h2>
        <div className="flex">
          <div className="w-2/5">
            <h3 className="mb-3 text-sm font-semibold text-primary">From</h3>
            <div className="flex gap-x-2 rounded border border-border">
              <span className="flex h-8 w-8 items-center justify-center text-sm font-bold text-primary">
                $
              </span>
              <input
                type="text"
                className="h-8 w-full border-none px-2 text-sm font-medium text-primary outline-none ring-transparent focus:outline-none focus:ring-transparent"
                placeholder="0"
              />
            </div>
          </div>
          <div className="flex w-1/5 items-end justify-center">
            <span className="pb-2 text-lg font-bold text-primary">-</span>
          </div>
          <div className="w-2/5">
            <h3 className="mb-3 text-sm font-semibold text-primary">TO</h3>
            <div className="flex gap-x-2 rounded border border-border">
              <span className="flex h-8 w-8 items-center justify-center text-sm font-bold text-primary">
                $
              </span>
              <input
                type="text"
                className="h-8 w-full border-none px-2 text-sm font-medium text-primary outline-none ring-transparent focus:outline-none focus:ring-transparent"
                placeholder="250.00"
              />
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default FilterPrice
