import React from "react"
import { AiOutlineArrowLeft, AiOutlineArrowRight } from "react-icons/ai"
import { BsChevronDown, BsGridFill, BsListUl } from "react-icons/bs"

import ProductCard from "@/components/cards/ProductCard"

const ProductLists = () => {
  return (
    <>
      <div>
        <TopRow />
        <div>
          <div className="grid grid-cols-3 gap-14">
            {[1, 2, 3, 4, 5, 6, 7, 8, 9].map((item, index) => (
              <ProductCard key={index} />
            ))}
          </div>
        </div>
        <div className="mt-8">
          <div className="flex justify-center bg-accent px-5 py-4">
            <div className="flex items-center gap-x-5">
              <span className="hover:text-secondary">
                <AiOutlineArrowLeft size={18} />
              </span>
              <button className="h-10 w-10 rounded-full border border-border bg-secondary p-2 text-white">
                1
              </button>
              <button className="h-10 w-10 rounded-full border border-border bg-white p-2 text-primary hover:bg-secondary hover:text-white">
                2
              </button>
              <button className="h-10 w-10 rounded-full border border-border bg-white p-2 text-primary hover:bg-secondary hover:text-white">
                3
              </button>
              <span className=" hover:text-secondary">
                <AiOutlineArrowRight size={18} />
              </span>
            </div>
          </div>
        </div>
      </div>
    </>
  )
}

export default ProductLists

const TopRow = () => {
  return (
    <>
      <div className="mb-6 flex items-center justify-between bg-accent px-5 py-3">
        <div className="flex items-center gap-x-7">
          <div className="flex items-center gap-x-2">
            <label className="mr-4 text-base font-semibold text-primary">
              Prev Page :
            </label>
            <select className="w-16 rounded border-border px-3 py-1 text-sm font-semibold text-primary">
              <option value="popularity">
                65 <BsChevronDown className="ml-3" />{" "}
              </option>
              <option value="rating">40</option>
              <option value="date">30</option>
              <option value="price">10</option>
            </select>
          </div>
          <div className="flex items-center gap-x-2">
            <label className="mr-4 text-base font-semibold text-primary">
              Sort By :
            </label>
            <select className="w-32 rounded border-border px-3 py-1 text-sm font-semibold text-primary">
              <option value="popularity">
                Sort by latest <BsChevronDown className="ml-3" />
              </option>
              <option value="rating">Rating</option>
              <option value="date">Date</option>
              <option value="price">Price</option>
            </select>
          </div>
          <div className="flex gap-x-2">
            <span className="rounded-md border border-secondary bg-white p-2 hover:border-secondary">
              <BsGridFill className="text-secondary " />
            </span>
            <span className="rounded-md border border-border bg-white p-2 hover:border-secondary">
              <BsListUl />
            </span>
          </div>
        </div>

        <p className="text-base font-semibold tracking-wide text-primary">
          Showing 1{"-"}12 of 53 results
        </p>
      </div>
    </>
  )
}
