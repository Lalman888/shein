"use client"

import React, { useState } from "react"

import { cn } from "@/lib/utils"

const BrandsFilter = () => {
  const [selected, setSelected] = useState<string[]>([])
  return (
    <>
      <div className="mr-9 rounded-sm border border-solid border-border px-4 py-5 shadow-widget-shadow">
        <h2 className="widget__title mb-6 font-frank text-base font-semibold">
          Filter By Price
        </h2>
        <div>
          <div className="grid grid-cols-3 gap-2">
            {[1, 2, 3, 4, 5, 6, 7, 8].map((item) => (
              <div
                key={item}
                className={cn(
                  "w-fit cursor-pointer rounded-sm border border-solid border-border px-4 py-2 hover:bg-secondary hover:text-white",
                  selected.includes(item.toString()) &&
                    "bg-secondary text-white"
                )}
                onClick={() => {
                  setSelected(
                    selected.includes(item.toString()) && selected.length > 1
                      ? selected.filter((i) => i !== item.toString())
                      : [...selected, item.toString()]
                  )
                }}
              >
                <span className="text-base">Brand {item}</span>
              </div>
            ))}
          </div>
        </div>
      </div>
    </>
  )
}

export default BrandsFilter
