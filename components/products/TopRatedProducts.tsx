import React from "react"
import Image from "next/image"
import { AiFillStar, AiOutlineStar } from "react-icons/ai"

const TopRatedProducts = () => {
  return (
    <>
      <div className="mr-9 rounded-sm border border-solid border-border px-4 py-5 shadow-widget-shadow">
        <h2 className="widget__title mb-6 font-frank text-base font-semibold">
          Top Rated Product
        </h2>
        <div>
          <TopProductCard />
          <TopProductCard />
          <TopProductCard />
        </div>
      </div>
    </>
  )
}

export default TopRatedProducts

const TopProductCard = () => {
  return (
    <>
      <div className="mb-3 flex gap-x-4">
        <div>
          <Image
            src="/img/product/small-product/product1.webp"
            alt="Picture of the author"
            width={100}
            height={86}
          />
        </div>
        <div>
          <h4 className="mb-2 font-frank text-base font-bold">White Cream</h4>
          <span className=" font-frank text-xs text-red-600">$ 100.00</span>
          <div className="mb-1 mt-2 flex gap-x-1">
            <AiFillStar className="text-yellow-500" />
            <AiFillStar className="text-yellow-500" />
            <AiFillStar className="text-yellow-500" />
            <AiFillStar className="text-yellow-500" />
            <AiOutlineStar className="text-yellow-500" />
          </div>
        </div>
      </div>
    </>
  )
}
