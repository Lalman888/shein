"use client"

import React from "react"

import { Checkbox } from "@/components/ui/checkbox"

const CategoriesNeeds = () => {
  return (
    <>
      <div className="mr-9 rounded-sm border border-solid border-border px-4 py-5 shadow-widget-shadow">
        <h2 className="widget__title mb-6 font-frank text-base font-semibold">
          Dietary Needs
        </h2>
        <ul className="widget__categories">
          {[1, 2, 3, 4].map((item) => (
            <NeedsCard key={item} />
          ))}
        </ul>
      </div>
    </>
  )
}

export default CategoriesNeeds

const NeedsCard = () => {
  return (
    <>
      <li className="group/need mb-3 flex justify-between space-x-2 border border-solid border-border px-4 py-2">
        <label
          htmlFor="terms"
          className="text-sm font-medium leading-none group-hover/need:text-secondary  peer-disabled:cursor-not-allowed peer-disabled:opacity-70"
        >
          Bath &amp; Body
        </label>
        <Checkbox id="terms" className="rounded-full border-secondary " />
      </li>
    </>
  )
}
