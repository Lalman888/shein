import React from "react"
import Image from "next/image"

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion"

const Categories = () => {
  return (
    <>
      <div className="mr-9 rounded-sm border border-solid border-border px-4 py-5 shadow-widget-shadow">
        <h2 className="widget__title mb-6 font-frank text-base font-semibold">
          Categories
        </h2>
        <ul className="widget__categories ">
          {[1, 2, 3, 4].map((item) => (
            <li key={item} className="mb-3 border border-solid border-border">
              <Accordion type="single" collapsible>
                <AccordionItem value="item-1">
                  <AccordionTrigger className="p-3">
                    <CategoriesHeadCard />
                  </AccordionTrigger>
                  <AccordionContent>
                    <CategoriesCard />
                    <CategoriesCard />
                    <CategoriesCard />
                    <CategoriesCard />
                    <CategoriesCard />
                  </AccordionContent>
                </AccordionItem>
              </Accordion>
            </li>
          ))}
        </ul>
      </div>
    </>
  )
}

export default Categories

const CategoriesHeadCard = () => {
  return (
    <>
      <div className="flex items-center">
        <div className="shrink-0">
          <Image
            src="/img/product/small-product/product1.webp"
            alt="product"
            width={25}
            height={21}
          />
        </div>
        <div className="ml-3">
          <p className="text-sm font-medium text-primary hover:text-secondary">
            Fairness Cream
          </p>
        </div>
      </div>
    </>
  )
}

const CategoriesCard = () => {
  return (
    <>
      <div className="flex items-center border-t p-3">
        <div className="shrink-0">
          <Image
            src="/img/product/small-product/product1.webp"
            alt="product"
            width={25}
            height={21}
          />
        </div>
        <div className="ml-3">
          <p className="text-sm font-medium text-primary hover:text-secondary">
            Fairness Cream
          </p>
        </div>
      </div>
    </>
  )
}
