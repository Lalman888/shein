import React from "react"
import Image from "next/image"
import { AiFillStar, AiOutlineStar } from "react-icons/ai"

const Testimonial = () => {
  return (
    <>
      <section className="bg-gray-200 pb-7 pt-20 ">
        <div className="container">
          <h1 className="mb-8 text-center font-frank text-2xl font-bold text-primary">
            What Clients Are Saying
          </h1>
          <div className="grid grid-cols-4 justify-between gap-8 pb-10">
            <TestimonialCard />
            <TestimonialCard />
            <TestimonialCard />
            <TestimonialCard />
          </div>
        </div>
      </section>
    </>
  )
}

export default Testimonial

const TestimonialCard = () => {
  return (
    <>
      <div className="relative rounded bg-white px-4 py-5">
        <div className="mb-3 flex items-center gap-x-4">
          <div>
            <Image
              src="/img/other/testimonial4.webp"
              width={70}
              height={70}
              alt="testimonial"
            />
          </div>
          <div>
            <h3 className="mb-1 font-frank text-sm font-semibold text-black">
              Lee Barners
            </h3>
            <span className="text-xs  text-primary ">Beautician</span>
            <div className="mt-1 flex items-center gap-x-1">
              <AiFillStar className="text-yellow-500" />
              <AiFillStar className="text-yellow-500" />
              <AiFillStar className="text-yellow-500" />
              <AiFillStar className="text-yellow-500" />
              <AiOutlineStar className="text-yellow-500" />
            </div>
          </div>
        </div>
        <div>
          <p className="mb-1 pr-8 text-xs font-normal leading-relaxed text-primary">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
            eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim
            ad minim veniam, quis nostrud
          </p>
          <div className="flex justify-end">
            <Image
              src="/img/icon/vector-icon.webp"
              width={40}
              height={35}
              alt="quote"
            />
          </div>
        </div>
      </div>
    </>
  )
}
