import React from "react"
import Image from "next/image"

const TeamMembers = () => {
  return (
    <>
      <section className="bg-white py-20">
        <div className="container">
          <h1 className="mb-8 font-frank text-2xl font-semibold text-primary">
            Our Team Member
          </h1>
          <div className="grid grid-cols-4 justify-between gap-4">
            <TeamMemberCard />
            <TeamMemberCard />
            <TeamMemberCard />
            <TeamMemberCard />
          </div>
        </div>
      </section>
    </>
  )
}

export default TeamMembers

const TeamMemberCard = () => {
  return (
    <>
      <div className="group/team mb-5">
        <div className="inline-block rounded-xl border-2 border-solid border-border p-1 group-hover/team:border-secondary">
          <Image
            src="/img/other/team1.webp"
            width={310}
            height={300}
            alt="team"
            className="rounded-xl"
          />
        </div>
        <div className="pt-4">
          <h3 className="mb-2 text-center font-frank text-base font-bold text-primary">
            Bruce Sutton
          </h3>
          <span className="flex justify-center text-sm font-normal text-secondary-foreground">
            CEO & Founder
          </span>
        </div>
      </div>
    </>
  )
}
