import {
  JetBrains_Mono as FontMono,
  Inter as FontSans,
  Frank_Ruhl_Libre,
} from "next/font/google"

export const frankRuhl = Frank_Ruhl_Libre({
  subsets: ["latin"],
  variable: "--font-frank-ruhl",
})

export const fontSans = FontSans({
  subsets: ["latin"],
  variable: "--font-sans",
})

export const fontMono = FontMono({
  subsets: ["latin"],
  variable: "--font-mono",
})
