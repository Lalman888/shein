export type SiteConfig = typeof siteConfig

export const siteConfig = {
  name: "Shein",
  description:
    "Shein is cosmetics and beauty products online store. We sell high quality products at affordable prices.",
}
