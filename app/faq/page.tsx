import React from "react"

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion"
import Services from "@/components/home/Services"

const Faq = () => {
  return (
    <>
      <section className="py-14">
        <div className="container">
          <div className="mb-7 border-b border-solid border-border pb-8">
            <h1 className="mb-6 font-frank text-xl font-bold text-primary">
              Shipping Information
            </h1>
            <div className="flex">
              <div className="mr-5 w-1/2">
                {[1, 2, 3].map((item, index) => (
                  <div className="mb-5" key={index}>
                    <Accordion type="single" collapsible>
                      <AccordionItem value="item-1">
                        <AccordionTrigger className="px-6 shadow-widget-shadow">
                          <h3 className="font-bold no-underline">
                            What Shipping Methods Are Available?
                          </h3>
                        </AccordionTrigger>
                        <AccordionContent className="px-6 py-4">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing
                          elit. Donec odio. Quisque volutpat mattis eros. Nullam
                          malesuada erat ut turpis. Suspendisse urna nibh,
                          viverra non, semper suscipit, posuere a, pede. Donec
                          nec justo eget felis facilisis fermentum. Aliquam
                          porttitor mauris sit amet orci. Aenean dignissim
                          felis.
                        </AccordionContent>
                      </AccordionItem>
                    </Accordion>
                  </div>
                ))}
              </div>
              <div className="mr-5 w-1/2">
                {[1, 2, 3].map((item, index) => (
                  <div className="mb-5" key={index}>
                    <Accordion type="single" collapsible>
                      <AccordionItem value="item-1">
                        <AccordionTrigger className="px-6 shadow-widget-shadow">
                          <h3 className="font-bold no-underline">
                            What Shipping Methods Are Available?
                          </h3>
                        </AccordionTrigger>
                        <AccordionContent className="px-6 py-4">
                          Lorem ipsum dolor sit amet, consectetuer adipiscing
                          elit. Donec odio. Quisque volutpat mattis eros. Nullam
                          malesuada erat ut turpis. Suspendisse urna nibh,
                          viverra non, semper suscipit, posuere a, pede. Donec
                          nec justo eget felis facilisis fermentum. Aliquam
                          porttitor mauris sit amet orci. Aenean dignissim
                          felis.
                        </AccordionContent>
                      </AccordionItem>
                    </Accordion>
                  </div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </section>
      <Services />
    </>
  )
}

export default Faq
