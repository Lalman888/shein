"use client"

import React from "react"
import Link from "next/link"

const NotFound = () => {
  return (
    <div>
      <div className="flex w-full justify-center bg-accent py-6 text-primary">
        <Link href="/">Home</Link>{" "}
        <span className="px-4 text-secondary">/</span> Error 404
      </div>
      <h1 className="text-2xl">404 - Page Not Found</h1>
      <Link href="/">
        <p className="text-primary">Go back home</p>
      </Link>
    </div>
  )
}

export default NotFound
