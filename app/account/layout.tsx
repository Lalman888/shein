export default async function AccountLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <div className="flex justify-center bg-accent py-6 text-primary">
        Home <span className="px-4 text-secondary">/</span> Account
      </div>
      <div>{children}</div>
    </>
  )
}
