import React from "react"

import Login from "@/components/account/Login"
import Register from "@/components/account/Register"
import Services from "@/components/home/Services"

const AcountPage = () => {
  return (
    <>
      <section className="pt-20">
        <div className="container ">
          <div className="flex gap-6">
            <div className="w-1/2">
              <Login />
            </div>
            <div className="w-1/2">
              <Register />
            </div>
          </div>
          <div>
            <Services />
          </div>
        </div>
      </section>
    </>
  )
}

export default AcountPage
