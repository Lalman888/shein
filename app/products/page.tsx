import React from "react"

import Services from "@/components/home/Services"
import BrandsFilter from "@/components/products/BrandsFilter"
import Categories from "@/components/products/Categories"
import CategoriesNeeds from "@/components/products/CategoriesNeeds"
import FilterPrice from "@/components/products/FilterPrice"
import ProductLists from "@/components/products/ProductLists"
import ShopByCategory from "@/components/products/ShopByCategory"
import TopRatedProducts from "@/components/products/TopRatedProducts"

const ProductList = () => {
  return (
    <>
      <section className="py-20">
        <div className="container ">
          <div className="w-full justify-center">
            <h2 className="pb-7 text-center font-frank text-3xl  font-bold">
              Shop By Category
            </h2>
            <ShopByCategory />
          </div>
        </div>
      </section>
      <section className="pt-10">
        <div className="container ">
          <div className="grid grid-cols-4 ">
            <div className="...">
              <div className="mb-14">
                <Categories />
              </div>
              <div className="mb-14">
                <CategoriesNeeds />
              </div>
              <div className="mb-14">
                <FilterPrice />
              </div>
              <div className="mb-14">
                <TopRatedProducts />
              </div>
              <div className="mb-14">
                <BrandsFilter />
              </div>
            </div>
            <div className="sticky top-0 col-span-3 ml-4">
              <ProductLists />
            </div>
          </div>
        </div>
      </section>
      <div>
        <Services />
      </div>
    </>
  )
}

export default ProductList
