export default async function WishListLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <div className="flex justify-center bg-accent py-6 text-primary">
        Home <span className="px-4 text-secondary">/</span> Wishlist
      </div>
      <div>{children}</div>
    </>
  )
}
