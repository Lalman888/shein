import React from "react"

import WishListCard from "@/components/cards/WishListCard"
import FeaturedProducts from "@/components/home/FeaturedProducts"
import Services from "@/components/home/Services"

const WhishListPage = () => {
  return (
    <>
      <section className="pt-20">
        <div className="container ">
          <h2 className="heading_2 mb-8">Wishlist</h2>
          <div>
            <table className="w-full">
              <thead className="text-primary">
                <tr>
                  <th className="pb-5 pr-5 text-left text-sm font-bold uppercase">
                    Product
                  </th>
                  <th className="pb-5 pr-5 text-left text-sm font-bold uppercase">
                    Price
                  </th>
                  <th className="pb-5 pr-5 text-left text-sm font-bold uppercase">
                    Stock Status
                  </th>
                  <th className="pb-5 pr-5 text-left text-sm font-bold uppercase">
                    Add to Cart
                  </th>
                </tr>
              </thead>
              <tbody>
                <WishListCard />
                <WishListCard />
                <WishListCard />
                <WishListCard />
              </tbody>
            </table>
            <div className="mb-4 flex items-center justify-between pt-5">
              <button className="text-primary">Continue shopping</button>
              <button className="text-primary">Clear Cart</button>
            </div>
          </div>
          <div>
            <FeaturedProducts />
          </div>
          <div>
            <Services />
          </div>
        </div>
      </section>
    </>
  )
}

export default WhishListPage
