"use client"

import { Affix, Transition, rem } from "@mantine/core"
import { useWindowScroll } from "@mantine/hooks"
import { AiOutlineArrowUp } from "react-icons/ai"

import HeroSlider from "@/components/ui/carousel/HeroSlider"
import About from "@/components/home/About"
import Blogs from "@/components/home/Blogs"
import FeaturedProducts from "@/components/home/FeaturedProducts"
import Offers from "@/components/home/Offers"
import Opening from "@/components/home/Opening"
import ProductsRow from "@/components/home/ProductsRow"
import Services from "@/components/home/Services"
import TrendingProduct from "@/components/home/TrendingProduct"

export default function IndexPage() {
  const [scroll, scrollTo] = useWindowScroll()
  return (
    <section>
      <HeroSlider />
      <Services />
      <About />
      <TrendingProduct />
      <Opening />
      <FeaturedProducts />
      <Offers />
      <ProductsRow />
      <Blogs />

      <div className="cursor-pointer">
        <Affix className="fixed bottom-0 right-0 mb-10 mr-10 rounded-full bg-secondary  text-white">
          <Transition transition="slide-up" mounted={scroll.y > 0}>
            {(transitionStyles) => (
              <div className="p-2">
                <AiOutlineArrowUp
                  className="h-6 w-6 cursor-pointer"
                  onClick={() => scrollTo({ y: 0 })}
                />
              </div>
            )}
          </Transition>
        </Affix>
      </div>
    </section>
  )
}
