import React from "react"
import Image from "next/image"

import { Button } from "@/components/ui/button"
import TeamMembers from "@/components/about/TeamMembers"
import Testimonial from "@/components/about/Testimonial"

const AboutPage = () => {
  return (
    <>
      <section className="py-14">
        <div className="container">
          <div className="flex gap-5 ">
            <div className="relative w-1/2 pl-20">
              <Image
                src="/img/other/about.webp"
                width={550}
                height={512}
                alt="about"
              />
              <div className="border_secondary absolute left-0 top-[36%] w-40 rounded-xl bg-white px-5 py-4 text-center ">
                <span className="mb-2 inline-block text-5xl font-extrabold text-primary">
                  <span className="font-frank">15</span>+
                </span>

                <span className="inline-block text-2xl font-medium text-primary">
                  Years <br /> Experience
                </span>
              </div>
            </div>
            <div className="flex w-1/2 flex-col justify-center gap-4">
              <h3 className="text-xl font-semibold text-secondary">About</h3>
              <h2 className="heading_2">Curated by color</h2>
              <p className="text-lg text-gray-500">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam,
              </p>
              <Button
                variant="secondary"
                size="lg"
                className="button_secondary w-fit"
              >
                View More
              </Button>
            </div>
          </div>
        </div>
      </section>
      <Achievements />
      <TeamMembers />
      <Testimonial />
    </>
  )
}

export default AboutPage

const Achievements = () => {
  return (
    <>
      <section className="bg-accent py-12">
        <div className="container">
          <div className="flex items-center justify-between gap-5">
            <div>
              <h2 className="mb-3 font-frank text-base font-semibold text-primary">
                YEARS OF <br />
                FOUNDATION
              </h2>
              <span className="text-4xl font-bold text-primary">50</span>
            </div>
            <div>
              <h2 className="mb-3 font-frank text-base font-semibold text-primary">
                SKILLED TEAM
                <br />
                MEMBERS
              </h2>
              <span className="text-4xl font-bold text-primary">100</span>
            </div>
            <div>
              <h2 className="mb-3 font-frank text-base font-semibold text-primary">
                HAPPY <br />
                CUSTOMERS
              </h2>
              <span className="text-4xl font-bold text-primary">80</span>
            </div>
            <div>
              <h2 className="mb-3 font-frank text-base font-semibold text-primary">
                MONTHLY <br />
                ORDERS
              </h2>
              <span className="text-4xl font-bold text-primary">70</span>
            </div>
          </div>
        </div>
      </section>
    </>
  )
}
