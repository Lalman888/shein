export default async function AboutLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <div className="flex justify-center bg-accent py-6 text-primary">
        Home <span className="px-4 text-secondary">/</span> About Us
      </div>
      <div>{children}</div>
    </>
  )
}
