export default async function ProductDetailList({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <div className="flex flex-col items-center bg-accent py-6 text-primary">
        <h2 className="pb-4 text-center font-frank text-2xl  font-bold">
          Products
        </h2>
        <div>
          Home <span className="px-4 text-secondary">/</span> Product Details
        </div>
      </div>
      <div>{children}</div>
    </>
  )
}
