import React from "react"
import Image from "next/image"
import { AiFillStar, AiOutlineHeart, AiOutlineStar } from "react-icons/ai"

import {
  Accordion,
  AccordionContent,
  AccordionItem,
  AccordionTrigger,
} from "@/components/ui/accordion"
import { Button } from "@/components/ui/button"
import Services from "@/components/home/Services"
import SuggestedProducts from "@/components/products/SuggestedProducts"

const ProductDetails = () => {
  return (
    <>
      <section className="pt-20">
        <div className="container">
          <div className="pb-20">
            <div className="flex gap-4">
              <div className="w-1/2">
                <div>
                  <Image
                    src="/img/product/big-product/product5.webp"
                    alt="product"
                    width={590}
                    height={563}
                  />
                </div>
                <div className="pt-7">
                  <div className="flex justify-between gap-x-2">
                    <div className="border border-solid border-border p-3">
                      <Image
                        src="/img/product/big-product/product5.webp"
                        alt="product"
                        width={114}
                        height={98}
                      />
                    </div>
                    <div className="border border-solid border-border p-3">
                      <Image
                        src="/img/product/big-product/product5.webp"
                        alt="product"
                        width={114}
                        height={98}
                      />
                    </div>
                    <div className="border border-solid border-border p-3">
                      <Image
                        src="/img/product/big-product/product5.webp"
                        alt="product"
                        width={114}
                        height={98}
                      />
                    </div>
                    <div className="border border-solid border-border p-3">
                      <Image
                        src="/img/product/big-product/product5.webp"
                        alt="product"
                        width={114}
                        height={98}
                      />
                    </div>
                  </div>
                </div>
              </div>
              {/* right */}
              <div className="w-1/2">
                <div>
                  <h2 className="mb-3 font-frank text-xl font-bold">
                    Beauty is Whatever Brings Perfect{" "}
                  </h2>
                  <div className="mb-3 flex gap-x-2">
                    <span className="text-lg font-bold text-red-600">
                      $ 200.00
                    </span>
                    <dl className="text-base text-secondary-foreground">
                      {" "}
                      $ 280.00
                    </dl>
                  </div>
                  <div className="mb-3 flex gap-x-1">
                    <div className="">
                      <AiFillStar className="text-yellow-400" />
                    </div>
                    <div className="">
                      <AiFillStar className="text-yellow-400" />
                    </div>
                    <div className="">
                      <AiFillStar className="text-yellow-400" />
                    </div>
                    <div className="">
                      <AiOutlineStar className="text-yellow-400" />
                    </div>
                    <div className="">
                      <AiOutlineStar className="text-yellow-400" />
                    </div>
                    <span className="text-xs font-normal text-secondary-foreground">
                      (126) Review
                    </span>
                  </div>
                  <p className="pb-4 pr-4 text-sm leading-relaxed text-primary">
                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Aut
                    numquam ullam is recusandae laborum explicabo id sequi
                    quisquam, ab sunt deleniti quidem ea animi facilis quod
                    nostrum odit! Repellendus voluptas suscipit cum harum.
                  </p>
                </div>
                <div className="mb-3">
                  <fieldset>
                    <legend className="mb-4 text-base font-bold">
                      Color :
                    </legend>
                    <div className="mb-3 flex gap-x-3">
                      {[1, 2, 3, 4].map((item) => (
                        <div
                          key={item}
                          className="border border-solid border-border p-3 hover:border-secondary"
                        >
                          <Image
                            src="/img/product/small-product/product4.webp"
                            alt="product"
                            width={20}
                            height={18}
                          />
                        </div>
                      ))}
                    </div>
                  </fieldset>
                </div>
                <div className="mb-3 flex items-center gap-x-7">
                  <div>
                    <div className="flex">
                      <button
                        className="mr-1 flex h-10 w-10 cursor-pointer items-center justify-center 
            rounded-l-lg bg-accent font-semibold shadow-xl"
                      >
                        -
                      </button>
                      <input
                        className="h-10 w-10 border border-solid border-y-border text-center text-sm"
                        type="text"
                        placeholder="1"
                        //   value="1"
                      />
                      <button
                        className="-mr-1 flex h-10 w-10 cursor-pointer items-center justify-center 
            rounded-r-lg bg-accent font-semibold shadow-xl"
                      >
                        +
                      </button>
                    </div>
                  </div>
                  <div>
                    <Button
                      variant="secondary"
                      size="lg"
                      className="button_secondary mr-2 w-fit "
                      style={{
                        textTransform: "capitalize",
                        fontSize: "12px",
                        padding: "1px 26px",
                      }}
                    >
                      Add to Cart
                    </Button>
                  </div>
                </div>
                <div className="mb-5 mt-3 flex items-center gap-x-3 text-primary hover:text-secondary">
                  <AiOutlineHeart className="text-2xl" />
                  <span className="text-lg font-semibold ">
                    Add to Wishlist
                  </span>
                </div>
                <div className="mb-3">
                  <Button
                    variant="secondary"
                    size="lg"
                    className="button_secondary w-full "
                    style={{ textTransform: "capitalize" }}
                  >
                    Buy it now
                  </Button>
                </div>
                <div className="mb-4 mt-1  flex gap-x-5">
                  <div>
                    <span>Social Share:</span>
                  </div>
                </div>
                <div className="mb-3">
                  <span className="mb-4 text-base font-bold text-primary">
                    Guaranteed Safe Checkout
                  </span>
                  <Image
                    src="/img/other/safe-checkout.webp"
                    alt="product"
                    width={355}
                    height={28}
                  />
                </div>
                <div>
                  <Accordion type="single" collapsible>
                    <AccordionItem value="item-1">
                      <AccordionTrigger className="px-3">
                        Description
                      </AccordionTrigger>
                      <AccordionContent>
                        Yes. It adheres to the WAI-ARIA design pattern.
                      </AccordionContent>
                    </AccordionItem>
                  </Accordion>
                </div>
                <div>
                  <Accordion type="single" collapsible>
                    <AccordionItem value="item-2">
                      <AccordionTrigger className="px-3">
                        Product Reviews
                      </AccordionTrigger>
                      <AccordionContent>
                        Yes. It adheres to the WAI-ARIA design pattern.
                      </AccordionContent>
                    </AccordionItem>
                  </Accordion>
                </div>
                <div>
                  <Accordion type="single" collapsible>
                    <AccordionItem value="item-3">
                      <AccordionTrigger className="px-3">
                        Additional Info
                      </AccordionTrigger>
                      <AccordionContent>
                        Yes. It adheres to the WAI-ARIA design pattern.
                      </AccordionContent>
                    </AccordionItem>
                  </Accordion>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
      <SuggestedProducts />
      <Services />
    </>
  )
}

export default ProductDetails
