import React from "react"

import ContactUs from "@/components/contact/Contact"
import Services from "@/components/home/Services"

const Contact = () => {
  return (
    <>
      <section className="py-20 ">
        <div className="container">
          <div className="mb-8">
            <h1 className="text-center font-frank text-3xl font-bold text-secondary">
              Get In Touch
            </h1>
            <p className="my-6 w-full text-center text-sm text-primary">
              Beyond more stoic this along goodness this sed wow manatee mongos
              flusterd impressive man farcrud opened.
            </p>
          </div>
          <div className="relative w-[90%] pb-8">
            <div className="rounded-xl py-11   pl-20 pr-8 shadow-contact-shadow md:ml-56 lg:ml-96">
              <h3 className="py-3 font-frank text-xl font-bold text-primary">
                Contact Me
              </h3>
              <ContactUs />
            </div>
            <div className="absolute left-12 top-12 w-96 rounded-xl bg-secondary px-14 py-10">
              <div className="mb-6">
                <h3 className="mb-4 font-frank text-xl font-semibold text-white">
                  Contact Us
                </h3>
                <div className="flex">
                  <div className="mr-4 pt-2 text-white">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="31.568"
                      height="31.128"
                      viewBox="0 0 31.568 31.128"
                    >
                      <path
                        id="ic_phone_forwarded_24px"
                        d="M26.676,16.564l7.892-7.782L26.676,1V5.669H20.362v6.226h6.314Zm3.157,7a18.162,18.162,0,0,1-5.635-.887,1.627,1.627,0,0,0-1.61.374l-3.472,3.424a23.585,23.585,0,0,1-10.4-10.257l3.472-3.44a1.48,1.48,0,0,0,.395-1.556,17.457,17.457,0,0,1-.9-5.556A1.572,1.572,0,0,0,10.1,4.113H4.578A1.572,1.572,0,0,0,3,5.669,26.645,26.645,0,0,0,29.832,32.128a1.572,1.572,0,0,0,1.578-1.556V25.124A1.572,1.572,0,0,0,29.832,23.568Z"
                        transform="translate(-3 -1)"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </div>
                  <div>
                    <p className="text-sm text-white">
                      Change the design through a range <br />
                      <span className="hover:text-primary">
                        +01234-567890
                      </span>{" "}
                      <span className="hover:text-primary">+01234-567890</span>
                    </p>
                  </div>
                </div>
              </div>
              <div className="mb-6">
                <h3 className="mb-4 font-frank text-xl font-semibold text-white">
                  Email Address
                </h3>
                <div className="flex">
                  <div className="mr-4 pt-2 text-white">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="31.57"
                      height="31.13"
                      viewBox="0 0 31.57 31.13"
                    >
                      <path
                        id="ic_email_24px"
                        d="M30.413,4H5.157C3.421,4,2.016,5.751,2.016,7.891L2,31.239c0,2.14,1.421,3.891,3.157,3.891H30.413c1.736,0,3.157-1.751,3.157-3.891V7.891C33.57,5.751,32.149,4,30.413,4Zm0,7.783L17.785,21.511,5.157,11.783V7.891l12.628,9.728L30.413,7.891Z"
                        transform="translate(-2 -4)"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </div>
                  <div>
                    <p className="text-sm text-white hover:text-primary">
                      info@example.com
                    </p>
                    <p className="text-sm text-white hover:text-primary">
                      info@example.com
                    </p>
                  </div>
                </div>
              </div>
              <div className="mb-6">
                <h3 className="mb-4 font-frank text-xl font-semibold text-white">
                  Office Location
                </h3>
                <div className="flex">
                  <div className="mr-4 pt-2 text-white">
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      width="31.57"
                      height="31.13"
                      viewBox="0 0 31.57 31.13"
                    >
                      <path
                        id="ic_account_balance_24px"
                        d="M5.323,14.341V24.718h4.985V14.341Zm9.969,0V24.718h4.985V14.341ZM2,32.13H33.57V27.683H2ZM25.262,14.341V24.718h4.985V14.341ZM17.785,1,2,8.412v2.965H33.57V8.412Z"
                        transform="translate(-2 -1)"
                        fill="currentColor"
                      ></path>
                    </svg>
                  </div>
                  <div>
                    <p className="text-sm text-white">
                      123 Stree New York City , United States <br />
                      Of America NY 750065.
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/* Map */}

        <div className="pb-2 pt-14">
          <iframe
            className="h-[500px] w-full"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d7887.465355142307!2d-0.13384360843222626!3d51.4876034467734!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48760532743b90e1%3A0x790260718555a20c!2sU.S.%20Embassy%2C%20London!5e0!3m2!1sen!2sbd!4v1632035375945!5m2!1sen!2sbd"
            style={{ border: 0 }}
            allowFullScreen={true}
            loading="lazy"
          ></iframe>
        </div>
        <Services />
      </section>
    </>
  )
}

export default Contact
