export default async function ContactLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <div className="flex justify-center bg-accent py-6 text-primary">
        Home <span className="px-4 text-secondary">/</span> Contact
      </div>
      <div>{children}</div>
    </>
  )
}
