export default async function CartLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <>
      <div className="flex justify-center bg-accent py-6 text-primary">
        Home <span className="px-4 text-secondary">/</span> Shopping Cart
      </div>
      <div>{children}</div>
    </>
  )
}
