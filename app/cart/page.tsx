import React from "react"

import { Button } from "@/components/ui/button"
import CartCard from "@/components/cards/CartCard"
import FeaturedProducts from "@/components/home/FeaturedProducts"
import Services from "@/components/home/Services"

const CartPage = () => {
  return (
    <>
      <section className="pt-20">
        <div className="container ">
          <h2 className="heading_2 mb-8">Shopping Cart</h2>
          <div className="grid grid-cols-3 gap-4">
            <div className=" col-span-2">
              <table className="w-full">
                <thead className="text-primary">
                  <tr>
                    <th className="pb-5 pr-5 text-left text-sm font-bold uppercase">
                      Product
                    </th>
                    <th className="pb-5 pr-5 text-left text-sm font-bold uppercase">
                      Price
                    </th>
                    <th className="pb-5 pr-5 text-left text-sm font-bold uppercase">
                      Quantity
                    </th>
                    <th className="pb-5 pr-5 text-left text-sm font-bold uppercase">
                      Total
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <CartCard />
                  <CartCard />
                  <CartCard />
                  <CartCard />
                </tbody>
              </table>
              <div className="mb-4 flex items-center justify-between pt-5">
                <button className="text-primary">Continue shopping</button>
                <button className="text-primary">Clear Cart</button>
              </div>
            </div>
            <div className="">
              <div className="sticky top-0 rounded-lg bg-white p-4 shadow-contact-shadow">
                <div className="mb-6">
                  <h3 className="mb-1 font-frank text-lg font-bold text-primary">
                    Coupon
                  </h3>
                  <p className="mb-3 text-sm text-secondary-foreground">
                    Enter your coupon code if you have one.
                  </p>
                  <div className="inline-flex items-center gap-5">
                    <input
                      type="text"
                      className="w-56 rounded-3xl  border border-solid border-secondary-foreground px-2 py-1 text-base"
                      placeholder="Enter your coupon code"
                    />
                    <Button
                      variant="secondary"
                      size="lg"
                      className="button_secondary mr-2 w-fit "
                      style={{
                        textTransform: "capitalize",
                        fontSize: "12px",
                        padding: "1px 26px",
                      }}
                    >
                      Apply Coupon
                    </Button>
                  </div>
                </div>
                <div className="mb-6">
                  <h3 className="mb-1 font-frank text-lg font-bold text-primary">
                    Note
                  </h3>
                  <p className="mb-3 text-sm text-primary">
                    Add special instructions for your seller...
                  </p>
                  <textarea
                    rows={6}
                    className="w-full  border border-solid border-secondary-foreground px-2 py-1 text-base hover:border-secondary"
                    placeholder="Add special instructions for your seller..."
                  />
                </div>
                <div className="mb-3 flex justify-between">
                  <span className="text-sm text-secondary-foreground">
                    SUBTOTAL
                  </span>
                  <span className="text-sm text-primary">$860.00</span>
                </div>
                <div className="mb-3 flex justify-between">
                  <span className="text-sm text-secondary-foreground">
                    GRAND TOTAL
                  </span>
                  <span className="text-sm text-primary">$860.00</span>
                </div>
                <p className="mb-3 text-sm text-primary">
                  Shipping & taxes calculated at checkout
                </p>
                <div className="flex justify-between">
                  <Button
                    variant="secondary"
                    size="lg"
                    className="button_secondary mr-2 w-fit "
                  >
                    Update Cart
                  </Button>
                  <Button
                    variant="secondary"
                    size="lg"
                    className="button_secondary mr-2 w-fit "
                  >
                    Check Out
                  </Button>
                </div>
              </div>
            </div>
          </div>
          <div>
            <FeaturedProducts />
          </div>
          <div>
            <Services />
          </div>
        </div>
      </section>
    </>
  )
}

export default CartPage
