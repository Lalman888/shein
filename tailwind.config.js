const { fontFamily } = require("tailwindcss/defaultTheme")

/** @type {import('tailwindcss').Config} */
module.exports = {
  darkMode: ["class"],
  content: ["app/**/*.{ts,tsx}", "components/**/*.{ts,tsx}"],
  theme: {
    container: {
      center: true,
      padding: "2rem",
      screens: {
        "2xl": "1400px",
        xl: "1200px",
        lg: "1024px",
        md: "768px",
      },
    },
    extend: {
      colors: {
        border: "var(--border)",
        input: "hsl(var(--input))",
        ring: "hsl(var(--ring))",
        background: "hsl(var(--background))",
        foreground: "var(--foreground)",
        primary: {
          DEFAULT: "var(--primary)",
          foreground: "var(--primary-foreground)",
        },
        secondary: {
          DEFAULT: "var(--secondary)",
          foreground: "var(--secondary-foreground)",
        },
        tertiary: {
          DEFAULT: "var(--tertiary)",
        },
        yellow: {
          DEFAULT: "var(--yellow)",
        },
        destructive: {
          DEFAULT: "var(--destructive)",
          foreground: "hsl(var(--destructive-foreground))",
        },
        muted: {
          DEFAULT: "hsl(var(--muted))",
          foreground: "hsl(var(--muted-foreground))",
        },
        accent: {
          DEFAULT: "var(--accent)",
          foreground: "hsl(var(--accent-foreground))",
        },
        popover: {
          DEFAULT: "hsl(var(--popover))",
          foreground: "hsl(var(--popover-foreground))",
        },
      },
      borderRadius: {
        lg: `var(--radius)`,
        md: `calc(var(--radius) - 2px)`,
        sm: "calc(var(--radius) - 4px)",
      },
      fontFamily: {
        // sans: ["var(--font-sans)", ...fontFamily.sans],
        frank: ["var(--frank-ruhl-fonts)"],
      },
      keyframes: {
        "accordion-down": {
          from: { height: 0 },
          to: { height: "var(--radix-accordion-content-height)" },
        },
        "accordion-up": {
          from: { height: "var(--radix-accordion-content-height)" },
          to: { height: 0 },
        },
        bideo_play: {
          "0%": {
            boxShadow: "0 0 0 0 rgba(201, 127, 95, 0.7)",
          },
          "40%": {
            boxShadow: "0 0 0 50px rgba(201, 127, 95, 0)",
          },
          "80%": {
            boxShadow: "0 0 0 50px rgba(201, 127, 95, 0)",
          },
          "100%": {
            boxShadow: "0 0 0 rgba(201, 127, 95, 0)",
          },
        },
        spinner: {
          to: {
            transform: "rotateZ(360deg)",
          },
        },
        letter_loading: {
          "0%": {
            transform: "rotateY(-90deg)",
            opacity: 0,
          },
          "25%": {
            transform: "rotateY(0deg)",
            opacity: 1,
          },
          "50%": {
            transform: "rotateY(0deg)",
            opacity: 1,
          },
          "75%": {
            transform: "rotateY(-90deg)",
            opacity: 0,
          },
          "100%": {
            transform: "rotateY(-90deg)",
            opacity: 0,
          },
        },
      },
      animation: {
        "accordion-down": "accordion-down 0.2s ease-out",
        "accordion-up": "accordion-up 0.2s ease-out",
        bideo_play: "bideo_play 2s linear infinite",
        spinner: "spinner 1s linear infinite",
        letter_loading: "letter_loading 5s infinite",
      },
      boxShadow: {
        "contact-shadow": "0 7px 20px rgba(0, 0, 0, 0.16)",
        "account-shadow": "0 5px 30px rgba(0, 0, 0, 0.1)",
        "widget-shadow": "0 2px 22px rgba(0, 0, 0, 0.1)",
      },
    },
  },
  plugins: [require("tailwindcss-animate"), require("@tailwindcss/forms")],
}
